#include "test_utilities.hh"

#include <iostream>

namespace utils {

  void print_timing(std::chrono::high_resolution_clock::duration runtime,
                    size_t n_iterations) {
    using namespace std::chrono;
    std::cout << "run time: "
              << duration_cast<milliseconds>(runtime).count() * 1e-3
              << " seconds" << std::endl;
    auto us = duration_cast<microseconds>(runtime / n_iterations).count();
    std::cout << "time per cycle: " << us * 1e-3 << " ms" << std::endl;
  }

}
