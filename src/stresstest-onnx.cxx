// this is a smoketest for onnx, should burn a lot of CPU

#include "test_utilities.hh"

#include <core/session/onnxruntime_cxx_api.h>

#include <valgrind/callgrind.h>

#include <iostream>
#include <fstream>
#include <chrono>
#include <map>
#include <cassert>

// main program and usage informaiton
void usage(const std::string& name) {
  std::cout << "usage: " << name << " <nn config> [n_iterations]\n"
    "\n"
    "The <nn config> file is from onnx\n"
    "\n";
}

// forward declare main test function
namespace {
  int run_on(Ort::Session& in_file, size_t n_iterations);
  void dump_info(Ort::Session& session);
}

int main(int argc, char* argv[]) {
  if (argc > 3 || argc < 2) {
    usage(argv[0]);
    exit(1);
  }
  // Read in the configuration.
  std::string in_file_name(argv[1]);

  // get onnx environment / allocator
  Ort::Env env;
  Ort::AllocatorWithDefaultOptions allocator;

  // get the session options, set up session
  Ort::SessionOptions so;
  so.SetIntraOpNumThreads( 1 );
  so.SetGraphOptimizationLevel( ORT_DISABLE_ALL );

  Ort::Session session(env, in_file_name.c_str(), so);

  dump_info(session);
  // here we need to build the onnx configuration

  size_t n_iterations = 1000;
  if (argc > 2) {
    n_iterations = std::stoll(argv[2]);
  }
  run_on(session, n_iterations);
  return 0;
}

namespace {
  // print some information
  void dump_info(Ort::Session& session) {
      size_t num_input_nodes = session.GetInputCount();
    std::cout << "looping over " << num_input_nodes << " input nodes"
              << std::endl;
    Ort::AllocatorWithDefaultOptions allocator;
    for (size_t iii = 0; iii < num_input_nodes; iii++) {
      std::cout << "input " << iii << ": "
                << session.GetInputName(iii, allocator) << std::endl;
      auto info = session.GetInputTypeInfo(iii).GetTensorTypeAndShapeInfo();
      std::cout << "type: " << info.GetElementType() << std::endl;
      std::cout << "shape:";
      for (auto dims: info.GetShape()) {
        std::cout << " " << dims;
      }
      std::cout << std::endl;
    }

  }

  struct ValueInfo {
    std::vector<int64_t> shape;
    std::vector<float> values;
    std::string name;
  };

  std::vector<ValueInfo> get_inputs(const Ort::Session& s) {
    Ort::AllocatorWithDefaultOptions allocator;
    std::vector<ValueInfo> info_vec;
    for (size_t iii = 0; iii < s.GetInputCount(); iii++) {
      ValueInfo info;
      info.shape = s.GetInputTypeInfo(iii)
        .GetTensorTypeAndShapeInfo().GetShape();
      // we have to set the first dimension to 1, since it's the batch size.
      info.shape.at(0) = 1;
      size_t n_elements = 1;
      for (auto d: info.shape) {
        n_elements *= d;
      }
      info.values = std::vector<float>(n_elements);
      info.name = s.GetInputName(iii, allocator);
      info_vec.push_back(info);
    }
    return info_vec;
  }

  template<typename T>
  std::vector<const char*> as_char(const T& i) {
    std::vector<const char*> chars;
    for (const auto& input: i) {
      chars.emplace_back(input.name.data());
    }
    return chars;
  }
  template<typename T>
  std::vector<Ort::Value> as_ort(const T& i) {
    auto mem_info = Ort::MemoryInfo::CreateCpu(
      OrtArenaAllocator, OrtMemTypeDefault);
    std::vector<Ort::Value> inputs;
    for (const ValueInfo& info: i) {
      std::vector<float> nonconst = info.values;
      inputs.emplace_back(
        Ort::Value::CreateTensor<float>(
          mem_info,
          nonconst.data(),
          info.values.size(),
          info.shape.data(),
          info.shape.size()));
    }
    return inputs;
  }

  std::map<std::string, Ort::Value> run_pattern(
    Ort::Session& s,
    const std::vector<ValueInfo>& input_info) {
    struct OutputInfo {
      std::vector<int64_t> shape;
      std::string name;
    };


    Ort::AllocatorWithDefaultOptions allocator;
    std::vector<OutputInfo> outputs;
    for (size_t iii = 0; iii < s.GetOutputCount(); iii++) {
      OutputInfo output;
      output.name = s.GetOutputName(iii, allocator);
      auto shape = s.GetOutputTypeInfo(iii)
        .GetTensorTypeAndShapeInfo().GetShape();
      output.shape = std::vector<int64_t>(begin(shape), end(shape));
      outputs.emplace_back(output);
    }
    auto inputs = as_ort(input_info);
    std::vector<const char*> inputs_as_char = as_char(input_info);
    std::vector<const char*> outputs_as_char = as_char(outputs);
    auto ort_out = s.Run(
      Ort::RunOptions{nullptr},
      inputs_as_char.data(),
      inputs.data(), inputs.size(),
      outputs_as_char.data(), outputs_as_char.size());
    assert(ort_out.size() == outputs.size());
    std::map<std::string, Ort::Value> pattern_out;
    for (size_t iii = 0; iii < outputs.size(); iii++) {
      pattern_out.emplace(outputs.at(iii).name,std::move(ort_out.at(iii)));
    }
    return pattern_out;
  }

  std::vector<float> as_vec(Ort::Value& v) {
    const float* floatarr = v.GetTensorMutableData<float>();
    size_t n = v.GetTensorTypeAndShapeInfo().GetElementCount();
    return std::vector<float>(floatarr, floatarr+n);
  }


  int run_on(Ort::Session& session, size_t n_iterations) {

    // build the a dummy input sequence
    auto input_info = get_inputs(session);

    // Loop over the output names once and compute the output for each
    // we don't profile in this first step because a lot of time is
    // spent loading libraries. Once they are loaded the code runs
    // much faster.
    std::cout << " --- running first pass ---" << std::endl;
    std::map<std::string, size_t> output_sizes;
    auto outputs = run_pattern(session, input_info);
    for (auto& pair: outputs) {
      std::cout << pair.first << ":";
      for (float element: as_vec(pair.second)) {
        output_sizes[pair.first]++;
        std::cout << " " << element;
      }
      std::cout << std::endl;
    }

    // now we do it a second time, things should be loaded into memory now
    std::cout << " --- running stress test ---" << std::endl;
    std::map<std::string, std::vector<float>> sum;
    for (const auto& pair: output_sizes) {
      sum[pair.first] = std::vector<float>(pair.second, 0);
    }

    CALLGRIND_START_INSTRUMENTATION;
    auto start = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < n_iterations; i++) {
      auto outputs = run_pattern(session, input_info);
      for (auto& pair: outputs) {
        std::vector<float>& ele = sum.at(pair.first);
        auto vec_outputs = as_vec(pair.second);
        for (size_t n = 0; n < ele.size(); n++) {
          ele.at(n) += vec_outputs.at(n);
        }
      }
    }
    auto stop = std::chrono::high_resolution_clock::now();
    CALLGRIND_STOP_INSTRUMENTATION;

    // print out some diagnostic info
    utils::print_timing(stop - start, n_iterations);

    // this last block is just to make sure the code does something
    // with the outputs
    std::cout << " --- sum of all outputs ---" << std::endl;
    for (const auto& out: sum) {
      std::cout << out.first << ":";
      for (const auto& val: out.second) {
        std::cout << " " << val;
      }
      std::cout << std::endl;
    }

    return 0;
  }


}
