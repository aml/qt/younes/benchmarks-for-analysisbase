#ifndef TEST_UTILITIES_HH
#define TEST_UTILITIES_HH

#include <chrono>

namespace utils {

  void print_timing(std::chrono::high_resolution_clock::duration runtime,
                    size_t n_iterations);

}

#endif
