// this is a smoketest for lwtnn, should burn a lot of CPU

#include "lwtnn_utilities.hh"
#include "test_utilities.hh"

#include "lwtnn/LightweightGraph.hh"
#include "lwtnn/parse_json.hh"
#include "lwtnn/Exceptions.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <valgrind/callgrind.h>

#include <iostream>
#include <fstream>
#include <chrono>

namespace {
  // forward declare some functions
  lwt::LightweightGraph::SeqNodeMap get_sequences(
    const std::vector<lwt::InputNodeConfig>& config);

  lwt::LightweightGraph::NodeMap get_scalars(
    const std::vector<lwt::InputNodeConfig>& config);
  int run_on(const lwt::GraphConfig& config, size_t n_iterations);
}

// main program and usage informaiton
void usage(const std::string& name) {
  std::cout << "usage: " << name << " <nn config> [n_iterations]\n"
    "\n"
    "The <nn config> file is from lwtnn\n"
    "\n";
}

int main(int argc, char* argv[]) {
  if (argc > 3 || argc < 2) {
    usage(argv[0]);
    exit(1);
  }
  // Read in the configuration.
  std::string in_file_name(argv[1]);
  std::ifstream in_file(in_file_name);
  auto config = lwt::parse_json_graph(in_file);

  size_t n_iterations = 1000;
  if (argc > 2) {
    n_iterations = std::stoll(argv[2]);
  }
  run_on(config, n_iterations);
  return 0;
}

namespace {




  int run_on(const lwt::GraphConfig& config, size_t n_iterations) {
    using namespace lwt;
    assert(config.outputs.size() > 0);

    // First build the tagger object. For graphs with multiple outputs
    // you must specify the name of the default output by name (thus
    // the second argument here).
    lwt::LightweightGraph tagger(config, config.outputs.begin()->first);

    // The inputs for sequences are defined like those for vectors,
    // but rather than a `double` each variable in the inner map is
    // represented by a vector.
    LightweightGraph::NodeMap in_nodes  = get_scalars(config.inputs);
    LightweightGraph::SeqNodeMap seq = get_sequences(config.input_sequences);

    // Loop over the output names once and compute the output for each
    // we don't profile in this first step because a lot of time is
    // spent loading libraries. Once they are loaded the code runs
    // much faster.
    std::cout << " --- running first pass ---" << std::endl;
    for (const auto& output: config.outputs) {
      auto out_vals = tagger.compute(in_nodes, seq, output.first);
      std::cout << output.first << ":" << std::endl;
      for (const auto& out: out_vals) {
        std::cout << out.first << " " << out.second << std::endl;
      }
    }
    // now we do it a second time, things should be loaded into memory now
    std::cout << " --- running stress test ---" << std::endl;
    std::map<std::string, double> sum;
    CALLGRIND_START_INSTRUMENTATION;
    auto start = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < n_iterations; i++) {
      for (const auto& output: config.outputs) {
        auto out_vals = tagger.compute(in_nodes, seq, output.first);
        for (const auto& out: out_vals) {
          sum[out.first] += out.second;
        }
      }
    }
    auto stop = std::chrono::high_resolution_clock::now();
    CALLGRIND_STOP_INSTRUMENTATION;

    // print out some diagnostic info
    utils::print_timing(stop - start, n_iterations);

    // this last block is just to make sure the code does something
    // with the outputs
    std::cout << " --- sum of all outputs ---" << std::endl;
    for (const auto& out: sum) {
      std::cout << out.first << " " << out.second << std::endl;
    }
    return 0;
  }

  lwt::LightweightGraph::SeqNodeMap get_sequences(
    const std::vector<lwt::InputNodeConfig>& config) {
    lwt::LightweightGraph::SeqNodeMap nodes;
    for (const auto& input: config) {
      // see the `test_utilities` header for this function.
      nodes[input.name] = get_values_vec(input.variables, 20);
    }
    return nodes;
  }

  lwt::LightweightGraph::NodeMap get_scalars(
    const std::vector<lwt::InputNodeConfig>& config) {
    // Build dummy input patterns from the configuration object. For
    // inputs that take vectors the inner map corresponds to the named
    // input variables for a node. The outer map indexes the nodes by
    // name. The `ramp` function is defined in the `test_utilities`
    // header.
    std::map<std::string, std::map<std::string, double> > in_nodes;
    for (const auto& input: config) {
      const size_t total_inputs = input.variables.size();
      std::map<std::string, double> in_vals;
      for (size_t nnn = 0; nnn < total_inputs; nnn++) {
        const auto& var = input.variables.at(nnn);
        double ramp_val = ramp(var, nnn, total_inputs);
        in_vals[var.name] = ramp_val;
      }
      in_nodes[input.name] = in_vals;
    }
    return in_nodes;
  }

}
