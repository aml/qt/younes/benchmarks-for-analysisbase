
cmake_minimum_required(VERSION 3.13)

# this seems to be required to set up anyting from a release
find_package(AnalysisBase 21.2 REQUIRED )

atlas_project( WorkDir 1.0 USE AnalysisBase ${AnalysisBase_VERSION} )

find_package( onnxruntime REQUIRED )

add_library(utils STATIC src/test_utilities.cxx)

add_executable(stresstest-lwtnn
  src/stresstest-lwtnn.cxx src/lwtnn_utilities.cxx)
# for some reason I need to include a package that _uses_ lwtnn, not
# lwtnn itself
target_link_libraries(stresstest-lwtnn FlavorTagDiscriminants utils)

add_executable(stresstest-onnx
  src/stresstest-onnx.cxx
  src/test_utilities.cxx)
# for some reason I need to include a package that _uses_ lwtnn, not
# lwtnn itself
target_link_libraries(stresstest-onnx utils ${ONNXRUNTIME_LIBRARIES})
target_include_directories(stresstest-onnx PRIVATE ${ONNXRUNTIME_INCLUDE_DIRS})

# we turn off pedantic warnings because onnx throws a lot
target_compile_options(stresstest-onnx PRIVATE -Wno-pedantic)
