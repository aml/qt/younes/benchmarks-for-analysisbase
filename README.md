Atlas Neural Network Benchmark Tests
====================================

This package contains some very basic code that you can use to
benchmark ATLAS neural networks.


Quick Start
===========

First you'll want to make sure you're working in an environment that
has everything you need. I started out with a docker image which
[you can find on gitlab][1].

[1]: https://gitlab.cern.ch/aml/qt/younes/container

Building
--------

Assuming you have that up and running, first get yourself to a clean
directory and clone this repository:

```
git clone ssh://git@gitlab.cern.ch:7999/aml/qt/younes/benchmarks-for-analysisbase.git
```

Next you'll want to try building it:

```
mkdir build
cd build
cmake ../benchmarks-for-analysisbase
make
```

Assuming everything went well, you should see two programs called
`stresstest-lwtnn` and `stresstest-onnx` in
`x86_64-centos7-gcc8-opt/bin`. These are the test programs we'll work
with. You can try running them with no arguments, they should just
print help.

Running
-------

The test scripts will ask for an "nn config" file. The code needs to
load in some neural network to run. We can get one with `wget`:

```
wget -q https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/201903/dl1r/antiktvr30rmax4rmin02track/network.json -O dl1r.json
```

This is the neural network that we use for flavor tagging. It's just a
text file (try looking at it with a text editor).

Ok, lets give this to `stresstest-lwtnn`:

```
./x86_64-centos7-gcc8-opt/bin/stresstest-lwtnn dl1r.json
```

This should print out something like

```
 --- running first pass ---
DL1r:
pb 0.205226
pc 0.427979
pu 0.366795
 --- running stress test ---
run time: 0.024 seconds
time per cycle: 0.024 ms
 --- sum of all outputs ---
pb 205.226
pc 427.979
pu 366.795
```

the numbers under "first pass" are the outputs from DL1r, which
correspond to flavor probabilities (when we give it a dummy jet). The
"stress test" block tells you how long the inference part of the code
ran for, and how long it took per cycle.

The last section is just to ensure that the compiler does something
with the outputs from the NN. Compilers are smart, if we don't use the
output it's liable to simply remove the code that produces them. The
result would run fast, but it wouldn't be very useful to us.

There is an analogous command for `*.onnx` files called
`stresstest-onnx`. We would like to compare both.

### Trying out other neural networks ###

You can [browse around the area][2] where you downloaded DL1r from
above. Some fun networks to try out:

- [The RNN input to DL1r][3]
- [The tau RNN][4]
- [The top ID NN][5]

[2]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/
[3]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/201903/rnnip/
[4]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/tauRecTools/00-03-00/
[5]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/BoostedJetTaggers/JSSWTopTaggerDNN/Rel21/

in all cases look for the `*.json` files, those are generally
compatible with `lwtnn`. You should notice that some networks run much
faster than others, generally the larger ones are slower.

### Generating Test Networks ###

The `make-simple-network.py` command will generate an example keras
network. Note that this must be run on a machine where tensorflow is
installed. For example, the command

```
./make-simple-network.py -l 30 100 80 50 3 -o example.h5
```

will generate a network with 30 inputs, a sequence of 100, 80, and 50
hidden units, and 3 outputs. I'll refer to this architecture as
"30-100-80-50-5".

These can be converted to both lwtnn and onnx models using the
[`keras2onnx` script][k2o].

[k2o]: https://gitlab.cern.ch/aml/containers/keras2onnx

Ideally we'd benchmark a few configurations:

 - many networks in ATLAS are relatively simple, and could be
   represented by a 10-5-4-1 layout.

 - Some more optimized NNs (i.e. in flavor tagging) are deeper, and
   could be similar to the 30-100-80-50-3 architecture mentioned above.

 - Modern NNs often have millions of parameters. They might follow an
   architecture like 100-1000-1000-1000-100-3.


Running callgrind
-----------------

Callgrind is a more sophisticated profiling tool than the simple
timing we did above. Assuming it exists in your environment, you can
invoke it with something like:

```
valgrind --tool=callgrind ./stresstest dl1r.json
```

This will generate some different output, and also a file with a name like

```
callgrind.out.658
```

the last number corresponds to the process ID that the operating
system assigned to `./stresstest`, it's not particularly
important.

I also wrapped this in a script called `run-callgrind.sh`. I run with a few more options enabled there, see that script for details.


Examining Callgrind Output
==========================

There are a few graphic tools to examine the output of callgrind,
mostly based on something called KCacheGrind. Personally I used
`qcachegrind` since it runs natively on OSX. I installed it with

```
brew install qcachegrind
```

You can also use `kcachegrind` on lxplus.
