#!/usr/bin/env python3

"""
Script to make dummy keras models
"""

from argparse import ArgumentParser
from pathlib import Path


def parse_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-f','--force', action='store_true')
    parser.add_argument('-o','--output', default='model.h5', type=Path)
    parser.add_argument('-l','--layers', nargs='+', type=int, default=[10,3])
    return parser.parse_args()

def build_model(layers):
    from tensorflow.keras import layers as l
    from tensorflow.keras import Model
    inputs = l.Input(shape=(layers[0],))
    x = inputs
    activation_functions = ['linear']*(len(layers) - 2) + ['softmax']
    for layer_dim, activation in zip(layers[1:], activation_functions):
        x = l.Dense(layer_dim, activation=activation)(x)
    model = Model(inputs=[inputs], outputs=[x])
    model.compile(loss='categorical_crossentropy')
    return model

if __name__ == '__main__':
    args = parse_args()
    if args.output.exists() and not args.force:
        exit(f'{args.output} already exists')
    args.output.parent.mkdir(exist_ok=True)
    model = build_model(args.layers)
    model.save(str(args.output))
