#!/usr/bin/env bash

# check for interactive session
if [[ $- == *i* ]]; then
    echo "don't source me!" >&2
    return 1
fi

# set to make sure the script quits if anything goes wrong
set -eu

# usage and help information
_usage() {
    echo "usage: ${0##*/} [-h] [-o <outext>] <program> [program-args]..."
}

CALLGRIND_OUTPUT_NAME=output.txt

_help() {
    _usage
    cat <<EOF

Simple script to run valgrind

Options:
 -o <outext>: output file extension

Output name will be prefixed by "callgrind.*", i.e. the full name will
be "callgrind.${CALLGRIND_OUTPUT_NAME}"

EOF
}

# parse inputs
while getopts ":ho:" opt $@; do
    case $opt in
        h) _help; exit 1;;
        o) CALLGRIND_OUTPUT_NAME=${OPTARG} ;;
        # handle errors
        \?) _usage; echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :) _usage; echo "Missing argument for -$OPTARG" >&2; exit 1;;
        *) _usage; echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done

REMAINING=("${@:OPTIND}")

# the first remaining argument is the file to run, it must exist.
if [[ ${#REMAINING[@]} -lt 1 ]] ; then
    _usage
    echo "no program given, quitting" >&2
    exit 1
fi

# this is being run with a few options:
#
#  --instr-atstart=no: tells valgrind not to collect any data as the
#    program is starting up. Useful to avoid profiling things that we
#    only run one time.
#
#  --simulate-cache=yes: tell callgrind to try to predict cache misses
#
#  --callgrind-out-file=XXX: specify an output file name, rather than
#    the auto-generated name.
#
valgrind \
    --instr-atstart=no\
    --simulate-cache=yes\
    --tool=callgrind\
    --callgrind-out-file=callgrind.${CALLGRIND_OUTPUT_NAME}\
    ${REMAINING[@]}
